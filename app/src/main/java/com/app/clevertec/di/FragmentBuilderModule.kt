package com.app.clevertec.di

import com.app.clevertec.ui.fragment.FormFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentBuilderModule {

    @ContributesAndroidInjector
    @FragmentScoped
    abstract fun contributeHomeFragment(): FormFragment
}
