package com.app.clevertec.di

import androidx.lifecycle.ViewModelProvider
import com.app.clevertec.ui.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
internal abstract class ViewModelBuilder {
    @ActivityScoped
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
