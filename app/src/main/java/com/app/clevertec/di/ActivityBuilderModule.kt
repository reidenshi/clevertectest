package com.app.clevertec.di

import com.app.clevertec.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ActivityScoped
    @ContributesAndroidInjector(
        modules =
        [
            FragmentBuilderModule::class,
            ViewModelBuilder::class
        ]
    )
    abstract fun contributeMainActivity(): MainActivity
}