package com.app.clevertec.di

import androidx.lifecycle.ViewModel
import com.app.clevertec.ui.viewmodel.FormViewModel
import com.app.clevertec.ui.viewmodel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FormViewModel::class)
    abstract fun bindFormViewModel(formViewModel: FormViewModel): ViewModel
}
