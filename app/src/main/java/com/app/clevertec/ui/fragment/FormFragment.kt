package com.app.clevertec.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.app.clevertec.R
import com.app.clevertec.data.dto.FormRequest
import com.app.clevertec.databinding.FragmentFormBinding
import com.app.clevertec.ui.adapter.FormAdapter
import com.app.clevertec.ui.utils.activityViewModelProvider
import com.app.clevertec.ui.utils.viewModelProvider
import com.app.clevertec.ui.viewmodel.FormViewModel
import com.app.clevertec.ui.viewmodel.MainViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class FormFragment : BaseFragment() {

    private lateinit var formViewModel: FormViewModel
    private lateinit var mainViewModel: MainViewModel
    private lateinit var viewBinding: FragmentFormBinding
    private var formAdapter : FormAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewBinding = FragmentFormBinding.inflate(layoutInflater)
        formViewModel = viewModelProvider()
        formViewModel.loadForm()

        formViewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                viewBinding.progressBar.visibility = VISIBLE
            } else {
                viewBinding.progressBar.visibility = GONE
            }
        }

        formViewModel.error.observe(viewLifecycleOwner) {
            context?.let { it1 ->
                MaterialAlertDialogBuilder(it1)
                    .setTitle(ERROR)
                    .setMessage(it)
                    .show()
            }
        }

        formViewModel.form.observe(viewLifecycleOwner) {
            val recycler = viewBinding.rvForm
            formAdapter = it.fields?.let { it1 -> FormAdapter(it1, ::onItemClicked) }
            recycler.adapter = formAdapter

            viewBinding.titleForm.text = it.title.toString()

            Glide.with(requireContext())
                .load(it.image)
                .fitCenter()
                .into(viewBinding.image)

            viewBinding.sendBtn.setOnClickListener {
                FormDialogFragment(::isAgreeClicked)
                    .show(requireFragmentManager(), FormDialogFragment.TAG)
            }
        }

        formViewModel.resultResponse.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it.result, Toast.LENGTH_LONG).show()
        }

        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModel = activityViewModelProvider()
    }

    private fun onItemClicked(values: List<String?>): ArrayAdapter<String?> {
        return ArrayAdapter(requireContext(), R.layout.list_item, values)
    }

    private fun isAgreeClicked(boolean: Boolean) {
        if (boolean) {
            formViewModel.sendForm(FormRequest(formAdapter?.getValues()))
            Log.e("log", "send")
        }
    }

    companion object {
        private const val ERROR = "Error"
    }
}
