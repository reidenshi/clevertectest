package com.app.clevertec.ui.utils

import android.text.Editable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.clevertec.ui.fragment.BaseFragment

inline fun <reified VM : ViewModel> BaseFragment.viewModelProvider() =
    ViewModelProvider(this, viewModelFactory).get(VM::class.java)

inline fun <reified VM : ViewModel> BaseFragment.activityViewModelProvider() =
    ViewModelProvider(requireActivity(), viewModelFactory).get(VM::class.java)

fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)
