package com.app.clevertec.ui.adapter

enum class ElementView {
    TEXT, NUMERIC, LIST
}
