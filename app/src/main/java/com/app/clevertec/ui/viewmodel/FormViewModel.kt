package com.app.clevertec.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.clevertec.data.FormDatabase
import com.app.clevertec.data.dto.FormRequest
import com.app.clevertec.data.dto.ResultResponse
import com.app.clevertec.data.entity.Form
import com.app.clevertec.data.repository.FormRepository
import com.app.clevertec.ui.utils.Utility
import kotlinx.coroutines.launch
import javax.inject.Inject

class FormViewModel @Inject constructor(
    private val utility: Utility,
    context: Context
) : ViewModel() {

    private val formRepository =
        FormRepository(FormDatabase.getDatabase(context.applicationContext))

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _form = MutableLiveData<Form>()
    val form: LiveData<Form> = _form

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _resultResponse = MutableLiveData<ResultResponse>()
    val resultResponse: LiveData<ResultResponse> = _resultResponse

    fun loadForm() {
        _isLoading.value = true
        viewModelScope.launch {
            try {
                val receivedForm = formRepository.getForm()
                _form.postValue(receivedForm)
                _isLoading.postValue(false)
            } catch (e: Exception) {
                e.message
            }
        }
    }

    fun sendForm(form: FormRequest) {
        _isLoading.value = true
        viewModelScope.launch {
            try {
                _resultResponse.postValue(formRepository.sendForm(form))
                _isLoading.postValue(false)
            } catch (e: Exception) {
                e.message
            }
        }
    }
}
