package com.app.clevertec.ui.fragment

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.app.clevertec.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class FormDialogFragment(
    private val onItemClicked: (Boolean) -> Unit
) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.form))
            .setView(R.layout.layout_cancel_dialog)
            .setPositiveButton(android.R.string.ok) { dialogInterface, i ->
                onItemClicked(true)
            }
            .setNegativeButton(android.R.string.cancel) { dialogInterface, i ->
                dismiss()
                onItemClicked(false)
            }
            .show()
    }

    override fun onStart() {
        super.onStart()
        dialog as androidx.appcompat.app.AlertDialog
    }

    companion object {
        const val TAG = "tag"
    }
}
