package com.app.clevertec.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.app.clevertec.ui.utils.Utility
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val utility: Utility,
    context: Context
) : ViewModel()
