package com.app.clevertec.ui.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.clevertec.R
import com.app.clevertec.data.dto.Field
import com.app.clevertec.ui.utils.toEditable

class FormAdapter(
    private val fields: List<Field>,
    private val onItemClicked: (List<String?>) -> ArrayAdapter<String?>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val currentValue: MutableMap<String, String> = mutableMapOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TEXT -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_form_text, parent, false)
                HolderText(view)
            }

            NUMERIC -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_form_numeric, parent, false)
                HolderNumeric(view)
            }

            LIST -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_form_list, parent, false)
                HolderList(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_form_text, parent, false)
                HolderText(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (fields[position].type) {
            ElementView.TEXT.toString() -> TEXT
            ElementView.NUMERIC.toString() -> NUMERIC
            ElementView.LIST.toString() -> LIST
            else -> {
                ERROR
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            TEXT -> {
                (holder as HolderText).bind(fields[position])
            }
            NUMERIC -> {
                (holder as HolderNumeric).bind(fields[position])
            }
            LIST -> {
                (holder as HolderList).bind(fields[position], onItemClicked)
            }
        }
    }

    override fun getItemCount(): Int = fields.size

    fun getValues() = currentValue

    inner class HolderText(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(field: Field) {
            view.findViewById<TextView>(R.id.title).text = field.title
            val editText = view.findViewById<EditText>(R.id.value)
            editTextProcess(editText, field)
        }
    }

    inner class HolderNumeric(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(field: Field) {
            view.findViewById<TextView>(R.id.title).text = field.title
            val editText = view.findViewById<EditText>(R.id.value)
            editTextProcess(editText, field)
        }
    }

    inner class HolderList(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(field: Field, onItemClicked: (List<String?>) -> ArrayAdapter<String?>) {
            view.findViewById<TextView>(R.id.title).text = field.title

            val items = field.values?.values?.toList()
            val keys = field.values?.keys?.toList()

            val listValues = view.findViewById<AutoCompleteTextView>(R.id.list_values)
            val adapter = items?.let { onItemClicked(it) }

            listValues.text = items?.get(0)?.toEditable()
            listValues?.setAdapter(adapter)

            listValues.setOnItemClickListener { parent, view, position, id ->
                keys?.get(position).let {
                    field.name?.let { it1 ->
                        if (it != null) {
                            currentValue[it1] = it
                        }
                    }
                }
            }
        }
    }

    private fun editTextProcess(editText: EditText, field: Field) {
        editText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {}

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                field.name?.let { currentValue.put(it, s.toString()) }
            }
        })
    }

    companion object {
        private const val TEXT = 0
        private const val NUMERIC = 1
        private const val LIST = 2
        private const val ERROR = 3
    }
}
