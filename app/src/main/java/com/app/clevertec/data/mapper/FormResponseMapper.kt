package com.app.clevertec.data.mapper

import com.app.clevertec.data.dto.FormResponse
import com.app.clevertec.data.entity.Form

class FormResponseMapper : Mapper<FormResponse, Form> {
    override fun map(from: FormResponse): Form {
        return Form(
            fields = from.fields.orEmpty(),
            image = from.image.orEmpty(),
            title = from.title.orEmpty(),
        )
    }
}
