package com.app.clevertec.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.app.clevertec.data.dao.FormTypeConverter
import com.app.clevertec.data.dto.Field

@Entity(tableName = "form_table")
@TypeConverters(FormTypeConverter::class)
class Form(
    val fields: List<Field>?,
    val image: String?,
    val title: String?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}
