package com.app.clevertec.data.dto

import com.google.gson.annotations.SerializedName

data class ResultResponse(
    @SerializedName("result")
    val result: String
)
