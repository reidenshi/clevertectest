package com.app.clevertec.data.repository

import com.app.clevertec.data.FormDatabase
import com.app.clevertec.data.dto.FormRequest
import com.app.clevertec.data.dto.ResultResponse
import com.app.clevertec.data.entity.Form
import com.app.clevertec.data.mapper.FormResponseMapper
import com.app.clevertec.data.networking.FormApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FormRepository(database: FormDatabase) {

    private val dao = database.formDao()
    private val ioScope = CoroutineScope(Dispatchers.IO)

    private val api = FormApi.provideRetrofit()
    private val formResponseMapper = FormResponseMapper()

    private suspend fun loadForm(): Form? {
        val response = api.loadForm()

        return if (response.isSuccessful) {
            response.body().let {
                it?.let { response -> formResponseMapper.map(response) }
            }
        } else {
            throw Throwable(response.errorBody().toString())
        }
    }

    private suspend fun insert() {
        ioScope.launch {
            try {
                loadForm()?.let { dao.insert(it) }
            } catch (e: Exception) {
                e.message
            }
        }
    }

    suspend fun sendForm(form: FormRequest): ResultResponse? {
        val response = api.sendForm(form)
        return if (response.isSuccessful) {
            response.body()
        } else {
            throw Throwable(response.errorBody().toString())
        }
    }

    suspend fun getForm(): Form {
        insert()
        delay(1000)
        return withContext(ioScope.coroutineContext) { dao.getForm() }
    }
}
