package com.app.clevertec.data.dto

import com.google.gson.annotations.SerializedName

data class FormRequest(
    @SerializedName("form")
    val form: MutableMap<String, String>?,
)
