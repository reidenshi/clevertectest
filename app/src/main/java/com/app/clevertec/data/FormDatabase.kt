package com.app.clevertec.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.app.clevertec.data.dao.FormDao
import com.app.clevertec.data.entity.Form

@Database(entities = [Form::class], version = 1)
abstract class FormDatabase : RoomDatabase() {

    abstract fun formDao(): FormDao

    companion object {
        var INSTANCE: FormDatabase? = null

        fun getDatabase(context: Context): FormDatabase {
            if (INSTANCE == null) {
                INSTANCE =
                    Room.databaseBuilder(context, FormDatabase::class.java, "Database").build()
            }
            return INSTANCE as FormDatabase
        }
    }
}
