package com.app.clevertec.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.app.clevertec.data.entity.Form

@Dao
interface FormDao {

    @Insert
    suspend fun insert(form: Form)

    @Query("SELECT * FROM form_table")
    suspend fun getForm() : Form
}
