package com.app.clevertec.data.dao

import androidx.room.TypeConverter
import com.app.clevertec.data.dto.Field
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class FormTypeConverter {

    @TypeConverter
    fun fromFieldsList(value:  List<Field>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Field>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toFieldList(value: String): List<Field> {
        val gson = Gson()
        val type = object : TypeToken<List<Field>>() {}.type
        return gson.fromJson(value, type)
    }
}
