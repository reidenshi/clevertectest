package com.app.clevertec.data.mapper

interface Mapper<F, T> {
    fun map(from: F): T
}
