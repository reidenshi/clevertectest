package com.app.clevertec.data.networking

import com.app.clevertec.data.dto.FormRequest
import com.app.clevertec.data.dto.FormResponse
import com.app.clevertec.data.dto.ResultResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface FormService {

    @GET("tt/meta/")
    suspend fun loadForm(): Response<FormResponse>

    @POST("tt/data/")
    suspend fun sendForm(@Body form : FormRequest) : Response<ResultResponse>
}
