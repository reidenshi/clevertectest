package com.app.clevertec.data.dto

import com.google.gson.annotations.SerializedName

data class Field(
    @SerializedName("name")
    val name: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("values")
    val values: MutableMap<String, String>?
)
