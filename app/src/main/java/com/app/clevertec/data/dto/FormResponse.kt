package com.app.clevertec.data.dto

import com.google.gson.annotations.SerializedName

data class FormResponse(
    @SerializedName("fields")
    val fields: List<Field>?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("title")
    val title: String?
)
